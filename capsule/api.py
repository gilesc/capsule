import os
import hashlib
import shutil

import docker
import yaml

from .util import LOG

DEFAULT_IMAGE = "registry.codeocean.com/codeocean/miniconda3:4.5.11-python3.7-cuda9.2-cudnn7-ubuntu18.04"

def get_docker_client():
    return docker.from_env()

class Capsule(object):
    def __init__(self, rootdir):
        self._root = os.path.abspath(os.path.expanduser(rootdir))

    @staticmethod
    def initialize(target_dir):
        if os.path.exists(target_dir) and len(os.listdir(target_dir)) > 0:
            raise Exception("Directory already exists and is non-empty.")

        templates_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "templates"))

        for dir in ["metadata", "environment", "code", "data", "results"]:
            p = os.path.join(target_dir, dir)
            os.makedirs(p, exist_ok=True)
               
        for root, dirs, files in os.walk(templates_dir):
            for fname in files:
                if fname == "__init__.py":
                    continue
                path = os.path.join(root, fname)
                rpath = os.path.relpath(path, start=templates_dir)
                target = os.path.join(target_dir, rpath)
                shutil.copyfile(path, target)
        
        return Capsule(target_dir)

    @property 
    def tag(self):
        dockerfile = os.path.join(self._root, "environment", "Dockerfile")
        md5 = hashlib.md5(open(dockerfile, "rb").read()).hexdigest()
        tag = f"capsule-{md5}"
        return tag


    def get_image(self):
        c = get_docker_client()
        dockerfile = os.path.join(self._root, "environment", "Dockerfile")
        tag = self.tag
        for image in c.images.list():
            if not isinstance(image, docker.models.images.Image):
                continue
            if self.tag in image.tags:
                LOG.info(f"Found Docker image: {image.id}")
                return image
        LOG.info(f"Docker image not found...building.")
        o = c.images.build(path=os.path.dirname(dockerfile), tag=tag)[0]
        LOG.info(f"Docker image built!")
        return o

    def run(self):
        image = self.get_image()
        c = get_docker_client()
        volumes = {os.path.join(self._root, k):{"bind": f"/{k}", "mode": "rw"} 
                for k in ["data", "code", "results"]}
        # FIXME: attach docker console?
        LOG.info("Executing capsule...")
        c.containers.run(image, command=["/bin/bash", "run.sh"],
                working_dir="/code",
                device_requests=[
                    docker.types.DeviceRequest(count=-1, capabilities=[['gpu']])
                ],
                remove=True, volumes=volumes)
