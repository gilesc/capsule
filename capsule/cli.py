import os

import click

from .api import Capsule

#DEFAULT_IMAGE

@click.group()
def cli():
    """
    CLI for managing Code Ocean format compute capsules.
    """
    pass

@cli.command()
@click.argument("capsule-root", nargs=1, default=os.getcwd())
def init(capsule_root):
    """
    Initialize a new capsule in this directory.
    """
    capsule = Capsule(capsule_root)
    capsule.initialize(capsule_root)

@cli.command()
@click.argument("capsule-root", nargs=1, default=os.getcwd())
def run(capsule_root):
    """
    Run the compute capsule in Docker.
    """
    capsule = Capsule(capsule_root)
    capsule.run()

def main():
    cli()

if __name__ == "__main__":
    main()
