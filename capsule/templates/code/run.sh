#!/usr/bin/env bash

set -ex
for file in *.ipynb; do
    jupyter nbconvert \
        --to html \
        --ExecutePreprocessor.allow_errors=True \
        --ExecutePreprocessor.timeout=-1 \
        --FilesWriter.build_directory=../results \
        --execute "$file"
done
